package com.edugaon.fruitslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fruitNamesArray = arrayOf("Apple", "Banana", "Guava", "orange", "Mango","Papaya","Litchi", "Watermelon", "Pineapple", "Kiwi","Greps")
        val fruitArrayAdapter = ArrayAdapter(this,R.layout.fruit_array_list_item, fruitNamesArray)
        val fruitArrayListView = findViewById<ListView>(R.id.fruits_listView)

        fruitArrayListView.adapter = fruitArrayAdapter
    }
}